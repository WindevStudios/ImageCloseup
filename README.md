# ImageCloseup
Have you ever wanted to see an image up close? ImageCloseup can help do that!  
With the power of Java, Windev Studios has created a basic app that can give you a singular window with an image that stretches! You can maximize it and fullscreen it!
# Configuration
Step one: Clone the repository with Git. Place it into a folder.  
Step two (optional, but recommended): Edit `image.java`. Replace the file location with the path to your image.  
Step three: Change directoried to your install directory (`cd path/to/ImageCloseup`) replacing `/path/to/ImageCloseup` with the path to ImageCloseup. Launch using `java app.java`. You'll see the image.  
Step four: You'll see the window pop up. It will display your image (the default one **if not preconfigured in image.java**).

